<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="./styles/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <title>Document</title>
</head>

<body>
    <div id="main">
        <div class="wrapper">
            <?php
            function checkDob($date, $format = 'd/m/Y')
            {
                $dataTime = DateTime::createFromFormat($format, $date);
                return $dataTime && $dataTime->format($format) === $date;
            }
            $yc = "";
            if (isset($_GET["submit"])) {
                if (empty($_GET["name"]))
                    $yc = $yc . '<div class="yc">Hãy nhập tên.<br></div>';
                if (empty($_GET["sex"]))
                    $yc = $yc . '<div class="yc">Hãy chọn giới tính.<br></div>';
                if (empty($_GET["khoa"]))
                    $yc = $yc . '<div class="yc">Hãy chọn phân khoa.<br></div>';
                if (empty($_GET["dob"]))
                    $yc = $yc . '<div class="yc">Hãy nhập ngày sinh.<br></div>';
                elseif (!checkDob($_GET["dob"]))
                    $yc = $yc . '<div class="yc">Hãy nhập ngày sinh đúng định dạng.<br></div>';
            }
            ?>
            <?php
            if (!empty($yc)) {
                echo $yc . "<br>";
            }
            ?>
            <form action="">
                <div class="form-group name-form">
                    <label for="" class="form-label">Họ và tên<span class="not-empty">*</span>
                    </label>
                    <input type="text" class="form-input" name="name">
                </div>
                <div class="form-group sex-form">
                    <label for="" class="form-label">Giới tính<span class="not-empty">*</span>
                    </label>
                    <?php
                    $arsex = ["Nam", "Nữ"];
                    for ($i = 0; $i < count($arsex); $i++) { ?>
                        <input type="radio" name="sex" class="radio-field" value=<?php echo $i; ?>>
                        <label for=""><?php echo $arsex[$i]; ?></label>
                    <?php }

                    ?>
                </div>
                <div class="form-group khoa-form">
                    <label for="khoa" class="form-label">Phân khoa<span class="not-empty">*</span>
                    </label>
                    <select name="khoa" id="khoa" class="khoa-select">
                        <?php
                        $arkhoa = ["", "MAT", "KDL"];
                        $phan_khoa = array(
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu"
                        );
                        foreach ($arkhoa as $khoa) {
                            echo ('<option value="' . $khoa . '">' . $phan_khoa[$khoa] . '</option>');
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group dob-form">
                    <label for="" class="form-label">Ngày sinh<span class="not-empty">*</span>
                    </label>
                    <div class='input-group date' id='dp'>
                        <input type='text' class="form-control" placeholder="dd/mm/yyyy" name="dob" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
                <div class="form-group address-form">
                    <label for="" class="form-label">Địa chỉ</label>
                    <input type="text" class="form-input" name="address">
                </div>

                <div class="button-form">
                    <button class="sub-btn" type="submit" name="submit">Đăng ký</button>
                </div>
            </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#dp').datetimepicker({
                    format: 'DD/MM/YYYY',
                })
            });
        </script>
    </div>

</body>

</html>